from setuptools import setup

from st_binding.version import __version__


setup(name='st_binding',
      version=__version__,
      packages=['st_binding'],
      install_requires=['numpy'],
      #download_url=['https://github.com/bendub/'],
      author='Benoit Dubois',
      author_email='dubois.benoit@gmail.com',
      description='Binding for Sigma Theta library',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)',
          'Natural Language :: English',
          'Programming Language :: Python',
          'Topic :: Scientific/Engineering'],
      zip_safe=False)
