# -*- coding: utf-8 -*-

"""package sigmatheta
author    Benoit Dubois
copyright Benoit Dubois, 2022
email     dubois.benoit@gmail.com
licence   GPL3+
brief     Wrapper over sigmatheta library.
"""

import sys
import ctypes as ct
import ctypes.util as ctu
from collections import OrderedDict
import numpy as np


LIBNAME = "sigmatheta"


def load_library(libname):
    """Portable load library function. Should be compatible with Cygwin,
    Windows and Cdecl compatible system (Solaris, Linux, FreeBSD, macOS...).
    """
    if sys.platform != 'cygwin':
        pathname = ctu.find_library(libname)
        if sys.platform == 'win32':
            # Windows backend uses stdcall calling convention
            lib = ct.WinDLL(pathname)
        else:
            # Standard cdecl calling convention
            lib = ct.CDLL(pathname)
    else:
        # Cygwin predefines library names with 'cyg' instead of 'libst.'
        try:
            lib = ct.CDLL("cyg" + libname + ".dll")
        except Exception as ex:
            raise OSError('Library could not be found') from ex
    return lib


libst = load_library(LIBNAME)


c_double_10 = ct.c_double * 10
c_double_128 = ct.c_double * 128
c_int_128 = ct.c_int * 128


DEVIANCE_TYPE = OrderedDict([("ADEV", 0),
                             ("MDEV", 1),
                             ("PDEV", 2),
                             ("HDEV", 3)])


class st_conf_int(ct.Structure):
    _fields_ = [
        ("bmin2s", ct.c_double),       # 95 % confidence low value
        ("bmin1s", ct.c_double),       # 68 % confidence low value
        ("bmax1s", ct.c_double),       # 68 % confidence high value
        ("bmax2s", ct.c_double)]       # 95 % confidence high value


st_asymptote_coeff = ct.c_double * 6
st_conf_int_128 = st_conf_int * 128


class st_serie(ct.Structure):
    _fields_ = [
        ("tau", c_double_128),          # Tau serie
        ("dev", c_double_128),          # Deviation estimate
        ("dev_unb", c_double_128),      # Unbiased estimate
        ("alpha", c_int_128),           # Dominating power law
        ("asym", st_asymptote_coeff),   # Asymptotes coefficients
        ("conf_int", st_conf_int_128),  # Confidence intervals
        ("length", ct.c_size_t)]        # Length of serie


class st_tau_inc(ct.Structure):
    _fields_ = [
        ("tau", c_double_10),
        ("length", ct.c_size_t)]

    def __init__(self, tau=c_double_10(1, 0, 0, 0, 0, 0, 0, 0, 0, 0), length=1):
        super().__init__(tau, length)


p_c_double = np.ctypeslib.ndpointer(dtype=ct.c_double, ndim=1,
                                    flags='C_CONTIGUOUS')
p_c_ubyte = np.ctypeslib.ndpointer(dtype=ct.c_ubyte, ndim=1,
                                   flags='C_CONTIGUOUS')
p_st_serie = ct.POINTER(st_serie)

lib_st_serie_dev = libst.st_serie_dev
lib_st_serie_dev.argtypes = [p_st_serie, st_tau_inc, ct.c_int,
                            ct.c_size_t, ct.c_int, p_c_double]
lib_st_serie_dev.restype = ct.c_int

lib_st_relatfit = libst.st_relatfit
lib_st_relatfit.argtypes = [p_st_serie, p_c_double, ct.c_int, p_c_ubyte]
lib_st_relatfit.restype = ct.c_int

lib_st_asym2alpha = libst.st_asym2alpha
lib_st_asym2alpha.argtypes = [p_st_serie, ct.c_ubyte]
lib_st_asym2alpha.restype = ct.c_int

lib_st_avardof = libst.st_avardof
lib_st_avardof.argtypes = [p_st_serie, p_c_double, ct.c_ubyte]
lib_st_avardof.restype = ct.c_int

lib_st_aduncert = libst.st_aduncert
lib_st_aduncert.argtypes = [p_st_serie, p_c_double, ct.c_ubyte]
lib_st_aduncert.restype = ct.c_int


def st_serie_dev(serie, ortau, devtype, n, tau, y):
    """Compute deviation of serie.
    :param serie: Pointer to st_serie data structure ()
    :param ortau: Tau increment structure ()
    :param devtype: index (see deviance_type) of deviation type to use (int)
    :param n: length of 1D arrays (int).
    :param t: a 1D array containing timetag data (numpy.array)
    :param y: a 1D array containing frequency fluctuation data (numpy.array)
    :returns: 0 in case of successful completion (int)
    """
    y = np.ascontiguousarray(y, dtype=np.double)
    return lib_st_serie_dev(ct.byref(serie), ortau,
                            ct.c_int(devtype), ct.c_size_t(n),
                            ct.c_int(tau), y)


def st_relatfit(serie, war, ord_, flag_slope):
    """
    """
    flag_slope = np.ascontiguousarray(flag_slope, dtype=np.dtype('B'))
    return lib_st_relatfit(ct.byref(serie), war, ord_, flag_slope)


def st_asym2alpha(serie, flag_variance):
    """
    """
    return lib_st_asym2alpha(ct.byref(serie), flag_variance)


def st_avardof(serie, edf, flag_variance):
    """
    """
    return lib_st_avardof(ct.byref(serie), edf, flag_variance)


def st_aduncert(serie, edf, flag_variance):
    """
    """
    return lib_st_aduncert(ct.byref(serie), edf, flag_variance)
